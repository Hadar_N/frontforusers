import React, { useRef, useState } from "react";
// import styled from "styled-components";
import axios from "axios";

import {usersUrl} from "../urlSRC"
import {Form,FormButton,FormInput,Container,ErrMsg} from "../styles/ComponentStyles"

const keys=["first_name", "last_name", "email"];

const AddUser = ({method, rec_id, changeCurr}) => {
    const formInputFields = useRef({});
    const [printMsg, setPrintMsg] = useState({isErr:false, msg:""});

    const handleClick = async (e) => {
        e.preventDefault();

        let toSendObj = {}
        for (let item of keys) {
            if(formInputFields.current[item].value) toSendObj[item] = (formInputFields.current[item].value);
        }

        try{

            if(method === "put" && rec_id) {
                console.log("put!", rec_id);

                const res = await axios.put(usersUrl + rec_id, toSendObj);

                if(res.status === 200) {
                    for (let item of keys) {
                        formInputFields.current[item].value = "";
                    }
                    setPrintMsg({isErr: false, msg:"changed successfully!"});
                    changeCurr(res.data);
                }

        } else {
            const res = await axios.post(usersUrl, toSendObj);

            if(res.status === 200) {
                for (let item of keys) {
                    formInputFields.current[item].value = "";
                }
                setPrintMsg({isErr: false, msg:"added successfully!"});
            }
        }

        }

        catch (e) {
            setPrintMsg({isErr: true, msg: e.response?.data?.stack});
        }

    }

    return (
        <Container>
            {method === "put" ? "change details:" : "add user:"}
        <Form>
            {keys.map(item => 
                <FormInput placeholder={item.split("_").join(" ")} key={item} ref={(elm) => formInputFields.current[item] = elm}/>
            )}

            <FormButton onClick={handleClick}>Submit</FormButton>

        </Form>

            <ErrMsg isErr={printMsg.isErr}>{printMsg.msg}</ErrMsg>
        </Container>
    )
}
export default AddUser;
