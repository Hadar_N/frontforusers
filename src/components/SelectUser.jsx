import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components";
import axios from "axios";

import {usersUrl} from "../urlSRC"
import AddUser from "./AddUser";

import {ErrMsg,Form,FormInput,FormButton, Container} from "../styles/ComponentStyles"

const SelectUser = () => {
    const [userRes, setUserRes] = useState();
    const [errMsg, setErrMsg] = useState("");
    const [deleteErrMsg, setDeleteErrMsg] = useState({isErr:false, msg:""});
    const inpField = useRef();

    useEffect(() => {setUserRes()},[setUserRes]);

    const handleSubmit = async (res) => {
        setUserRes(res);
    }


    const handleClick = async (e) => {
        e.preventDefault();
        if(inpField.current.value)
        try{
        let data = await axios.get(usersUrl + `${inpField.current.value}`);
        setUserRes(data.data);
        setErrMsg("");
        } catch (e) {
            if(e.response?.status === 500) {setErrMsg("id not valid!"); setUserRes()}
            else if(e.response?.status === 404) {setErrMsg("no such user"); setUserRes()}
        }
        else {setErrMsg("please search id"); setUserRes()}
    }

    const handleDelete = async (e) => {
        e.preventDefault();
        try{
        let res = await axios.delete(usersUrl + `${userRes._id}`);
        if(res===null) {setDeleteErrMsg({isErr:true, msg:"no such user"})}
        else {
            setDeleteErrMsg({isErr:false, msg:"deleted seccessfully!"});
            window.setTimeout( () => {
            setUserRes()
            inpField.current.value="";
            },1000);
        }
        } catch (e) {
            if(e.response?.status === 500) {setDeleteErrMsg({isErr:true, msg:"id not valid!"})}
        }
    }

    const printUserObj = (obj) => {
        if(obj)
        return (<div key={obj._id}>
            {obj.first_name} {obj.last_name},
            <br />
            {obj.email}
            </div>)
        else return <></>
    }

    return (
        <Container>
            write user id:
            <Form>
            <FormInput placeholder="user id" ref={inpField}/>
            <FormButton onClick={handleClick}>search</FormButton>
            </Form>

<p />
            {printUserObj(userRes)}
            <ErrMsg isErr={true}>{errMsg}</ErrMsg>

            {userRes && <>
            <AddUser method="put" rec_id={userRes._id} changeCurr={handleSubmit}/>

            or
            <br/>
            <DelBtn onClick={handleDelete}>Delete User</DelBtn>
            <br/>
            <ErrMsg isErr={deleteErrMsg.isErr}>{deleteErrMsg.msg}</ErrMsg>
            </>}
            
        </Container>
    )
}
export default SelectUser;


const DelBtn = styled.button`
background: red;
border: 2px solid black;
font-weight: bold;
font-size: 1.3rem;
margin: .5rem;
`;