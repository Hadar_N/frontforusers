import React from "react";
import styled from "styled-components";
import {Link} from "react-router-dom";

const TopBar = () => {

    return (
        <Menu>
            <Link to="/">Home</Link>
            <Link to="/UsersList">Users List</Link>
            <Link to="/AddUser">Add User</Link>
            <Link to="/SelectUser">Edit User</Link>
        </Menu>
    )
}
export default TopBar;

const Menu = styled.div`
width: 100%;
padding: 1rem;
background-color: rgba(100,150,200,0.5);
position: sticky;
top: 0;

display: flex;
justify-content: space-evenly;
`;