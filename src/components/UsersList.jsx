import React, { useEffect, useState } from "react";
import styled from "styled-components";
import axios from "axios";

import {usersUrl} from "../urlSRC"
import {Container } from "../styles/ComponentStyles"

const UsersList = () => {
    const [completeList, setCompleteList] = useState([]);

    useEffect(() => {
        const getData = async () => {
            let data = await axios.get(usersUrl);
            setCompleteList(data.data);
        }
        getData();
    },[setCompleteList])

    return (
        <Container>
        users list:
        <List>
            {completeList.map(item => 
                <li key={item._id}>
                    {item.first_name} {item.last_name},
                <br />
                {item.email}
                </li>
            )}
        </List>
        </Container>
    )
}
export default UsersList;

const List = styled.ul`
list-style-type: none;
padding: 0;

li {
    padding: .7rem ;
}
`;