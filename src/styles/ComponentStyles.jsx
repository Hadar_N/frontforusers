import styled, {css} from "styled-components";

export const centering = css`
display: flex;
flex-direction: column;
align-items: center;
`;
export const Container = styled.div`
${centering}
`;
export const Form = styled.form`
    ${centering}
    gap: .5rem;
    padding: 1rem;
    width:15rem;
`;

export const FormInput = styled.input`
    width: 100%;
`;
export const FormButton = styled.button`
    width: 30%;
`;

export const ErrMsg = styled.h5`
color: ${({isErr}) => isErr? "red" : "lime"};
`;