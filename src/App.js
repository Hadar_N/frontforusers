import './styles/App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom"

import TopBar from "./components/TopBar"
import Main from "./components/Home"
import UsersList from "./components/UsersList"
import AddUser from "./components/AddUser"
import SelectUser from "./components/SelectUser"

function App() {
  return (
    <div className="App">
      <header className="App-header">

        <BrowserRouter>
          <TopBar />

          <Switch>
            <Route exact path="/" component={Main}/>
            <Route path="/UsersList" component={UsersList}/>
            <Route path="/AddUser" component={AddUser}/>
            <Route path="/SelectUser" component={SelectUser}/>
          </Switch>
        </BrowserRouter>

      </header>
    </div>
  );
}

export default App;
